function! SourceIfExists(file)
  if filereadable(expand(a:file))
    exe 'source' a:file
  endif
endfunction

" required configuration
set nocompatible		" be iMproved, required
filetype off			" required

" All of your Plugins must be added before the following line
"call vundle#end()            " required
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
filetype plugin on

" basics
"set nocompatible				" use Vim defaults - already set
set encoding=utf-8				" use UTF-8 encoding
set fileencoding=utf-8  		" use UTF-8 encoding to write files
set mouse=a						" make sure mouse is used in all cases.
set equalalways					" alway split windows equally
set completeopt=menuone			" enable completion menu for one item
								" don't use longest, it behaves like
								" autocorrection shit, can't delete
								" autocompleted items
set wildmode=longest:list,full	" first tab - complete as much as posible
								" and show all completion obpions
								" second tab - show menu and select first
set wildmenu					" looks like much better option of wildmode
set cursorline					" allows line nuber to be highlighted
								" and sets dark backgroud for line
								" with cursor
set cursorcolumn				" sets dark backgroud for cursor column
set clipboard+=unnamed			" yank and copy to X clipboard
set backspace=2					" full backspacing capabilities
set history=256					" 256 lines of command line history
set viminfo='20,\"50			" read/write a .viminfo file, don't store more
								" than 50 lines of registers
set ruler						" cursor position display in status line
set showmode					" show mode at bottom of screen
set showcmd						" show commands at bottom of screen
set showmatch					" show matching brackets (),{},[]
set number						" display row numbers
set laststatus=2				" permanently display status line
set timeoutlen=250				" Time to wait after ESC (default causes an annoying delay)
set magic						" enable extended regular expressions
set tags+=.tags;~		"	search for ctags up to $HOME

" searching and search highlighting
set hlsearch			" highlight all search results
set incsearch			" increment search
set ignorecase			" case-insensitive search
set smartcase			" upper-case sensitive search

" syntax highlighting and color scheme
syntax on				" enable syntax highlighting
set background=dark		" improve default color scheme
color vaclavz			" my custom color scheme
set t_Co=256			" 256 colors - tmux compatibility

" plug-in settings
filetype plugin on
filetype indent on

" disable replace mode on insert double press
imap <Insert> <Nop>
inoremap <S-Insert> <Insert>

" ctrl+n with popup completion window behaves like down arrow
inoremap <expr> <C-n> pumvisible() ? '<C-n>' : '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" tab customizations
set shiftround			" round indent to multiple of 'shiftwidth'
set autoindent			" align the new line indent with the previous line
set shiftwidth=2		" operation >> indents 2 columns; << unindents 4 columns
set tabstop=2			" a hard TAB displays as 2 columns
set softtabstop=2		" insert/delete 2 spaces when hitting a TAB/BACKSPACE
set smarttab			" deletes whole inserted tab
" set expandtab			" insert spaces when hitting TABs

" fold cutomizations
" folding is controlled with za, zo, zc etc.
set foldmethod=indent
set foldlevel=99

" upravena definice vim sessionu, aby se nevkladalo cd do cilove slozky,
" neloadovaly se nezobrazene buffery a neukladaly se foldy, ktere akorat
" zdrzuji pri nacitani vimu
set sessionoptions=sesdir,options,tabpages,winsize

" disable default ctrl+j behavior
let g:BASH_Ctrl_j = 'off'

" custom windows switching
" predelano z puvodniho map, coz obcas sezral terminal
" premapovani ctrl + hjkl
nnoremap <c-j> <c-w>j	" press ctrl + j to switch to down windows
nnoremap <c-k> <c-w>k	" press ctrl + k to switch to top windows
nnoremap <c-l> <c-w>l	" press ctrl + l to switch to right windows
nnoremap <c-h> <c-w>h	" press ctrl + h to switch to left windows

" custop tab switching
nnoremap <c-p> :tabp<CR>		" press ctrl + p to switch to previous tab
nnoremap <c-n> :tabn<CR>		" press ctrl + n to switch to next tab

" When editing a file, always jump to the last cursor position
autocmd BufReadPost *
\ if line("'\"") > 0 && line ("'\"") <= line("$") |
\   exe "normal! g'\"" |
\ endif

" don't write swapfile on most commonly used directories for NFS mounts or USB sticks
autocmd BufNewFile,BufReadPre /media/*,/run/media/*,/mnt/* set directory=~/tmp,/var/tmp,/tmp

augroup logs
	" soubory s priponou .log se automaticky nacitaji s filetype=messages
	autocmd BufEnter,BufReadPre /var/log/* set filetype=messages
	autocmd BufEnter,BufReadPre /var/log/* set filetype=vaclavz
augroup END

" F2 - vypne automaticke odsazovani a tabulatorovani pri vkladani textu
set pastetoggle=<F2>
autocmd filetype * nnoremap <F6> :belowright terminal<CR>

" omezeni sirky a barevne zvyrazneni pri jejim prekroceni - vertical split
" screen na fullhd
augroup split_screen_limit
	" For all text files set 'textwidth' to 112 characters.
	autocmd FileType cpp,hpp,c,h,md,py setlocal textwidth=112

	" highlight lines longer than 112 chars
	autocmd FileType cpp,hpp,c,h,md,py highlight OverLength ctermbg=172
	autocmd FileType cpp,hpp,c,h,md,py match OverLength /\%112v.*/ |
augroup END
