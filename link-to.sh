#!/bin/bash
set -o pipefail

VERSION_MAJOR=1
VERSION_MINOR=0
VERSION_PATCH=0
VERSION="$VERSION_MAJOR.$VERSION_MINOR.$VERSION_PATCH"

TARGET_DIR="UNDEFINED"

while [ "$#" -gt 0 ]
do
	if [ "$1" = "-V" ] || [ "$1" = "--version" ]
	then
		echo "$VERSION"
		exit 0
	elif [ "$1" = "-h" ] || [ "$1" = "--help" ]
	then
		echo -e "Link all necessary dotfiles to requested directory
	parameters:
	-V | --version\t\t\tprint script version
	-h | --help\t\t\tprint script help
	-t /PATH/TO/DIR | --target-dir /PATH/TO/DIR\t\t\tparameter for target directory
	-x\t\t\t\t\tdebug output of the script"
		exit 0
	elif [ "$1" = "-t" ] || [ "$1" = "--target-dir" ]
	then
		shift
		if [ -z "$1" ]
		then
			echo "Missing parameter for target dir"
			exit 1
		fi

		# set target dir, strip trailing slash
		TARGET_DIR="${1%/}"
	# debug output of the script
	elif [ "$1" = "-x" ]
	then
		echo "Detected parameter -x for debug output"
		set -x
	else
		echo "Unsupported parameter: $1"
		exit 1
	fi

	shift
done

if [ "$TARGET_DIR" = "UNDEFINED" ]
then
	echo "Unable to link, no target folder defined in parameter"
	exit 1
elif [ -z "$TARGET_DIR" ]
then
	echo "Invalid parameter, no folder detected: ($TARGET_DIR)"
	exit 1
elif [ ! -d "$TARGET_DIR" ]
then
	echo "Requested folder doesn't exist"
	exit 1
else
	echo "Linking dotfiles to folder: $TARGET_DIR"
fi


rm -r "$TARGET_DIR"/.tmux &>/dev/null; ln -sv /usr/local/etc/dotfiles/.tmux "$TARGET_DIR"/ || echo "unable to link .tmux"
rm "$TARGET_DIR"/.tmux.conf &>/dev/null; ln -sv /usr/local/etc/dotfiles/.tmux.conf "$TARGET_DIR"/ || echo "unable to link .tmux.conf"
rm -r "$TARGET_DIR"/.vim &>/dev/null; ln -sv /usr/local/etc/dotfiles/.vim "$TARGET_DIR"/ || echo "unable to link .vim"
rm "$TARGET_DIR"/.vimrc &>/dev/null; ln -sv /usr/local/etc/dotfiles/.vimrc "$TARGET_DIR"/ || echo "unable to link .vimrc"
rm -r "$TARGET_DIR"/.zsh &>/dev/null; ln -sv /usr/local/etc/dotfiles/.zsh "$TARGET_DIR"/ || echo "unable to link .zsh"
rm "$TARGET_DIR"/.zshrc &>/dev/null; ln -sv /usr/local/etc/dotfiles/.zshrc "$TARGET_DIR"/ || echo "unable to link .zshrc"
rm "$TARGET_DIR"/.zshenv &>/dev/null; ln -sv /usr/local/etc/dotfiles/.zshenv "$TARGET_DIR"/ || echo "unable to link .zshenv"
rm "$TARGET_DIR"/.gdbinit &>/dev/null; ln -sv /usr/local/etc/dotfiles/.gdbinit "$TARGET_DIR"/ || echo "unable to link .gdbinit"
rm "$TARGET_DIR"/.pystartup &>/dev/null; ln -sv /usr/local/etc/dotfiles/.pystartup "$TARGET_DIR"/ || echo "unable to link .pystartup"
touch "$TARGET_DIR"/.pgpass && chmod 600 "$TARGET_DIR"/.pgpass